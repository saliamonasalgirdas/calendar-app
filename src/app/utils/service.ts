export class RestfulService {

  static put(url, data) {
    return fetch(`${ 'http://localhost:3000' }${ url }`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(data)
    })
      .catch(error => console.error('Patch error =\n', error));
  }

  static post(url, data) {
    return fetch(`${ 'http://localhost:3000' }${ url }`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(data)
    })
      .catch(error => console.error('Patch error =\n', error));
  }

  static get(url) {
    return fetch(`${ 'http://localhost:3000' }${ url }`)
      .then(response => response.json())
      .catch(error => console.error('Fetch error =\n', error));
  }

  static delete(url, data) {
    return fetch(`${ 'http://localhost:3000' }${ url }`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .catch(error => console.error('Fetch error =\n', error));
  }
}
