import {Component, OnInit, Input} from '@angular/core';
import {IEvent} from '../ievent';
import {CalendarComponent} from '../calendar.component';


@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit {

  DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  // @Input() dayEvents: IEvent;
  @Input() currentMonth: number;
  @Input() events: IEvent;
  @Input() passedDay: number;

  constructor() {
  }

  ngOnInit() {
  }

  openEvent() {

  }
}
