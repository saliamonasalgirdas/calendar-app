import {Component, OnInit, Input} from '@angular/core';
import {IEvent} from '../../ievent';

@Component({
  selector: 'app-calendar-day-event',
  templateUrl: './calendar-day-event.component.html',
  styleUrls: ['./calendar-day-event.component.css']
})
export class CalendarDayEventComponent implements OnInit {

  @Input() dayEvents: IEvent;
  constructor() { }

  ngOnInit() {
    console.log(this.dayEvents);
  }

}
