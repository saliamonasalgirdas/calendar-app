import { Pipe, PipeTransform} from '@angular/core';
import {IEvent} from '../../ievent';

@Pipe({name: 'eventPipe'})
export class DayEventFilter implements PipeTransform {
  transform(value: Array<IEvent>, day: number, month: number): IEvent[] {
    return  value.filter(item => (+item.date.split('-')[2] === day) && (+item.date.split('-')[1] === month + 1));
  }
}
