import {Component, OnInit} from '@angular/core';
import {IEvent} from './ievent';
import {UtilsService} from './utils.service';
import {DayEventFilter} from './day/calendar-day-event/eventpipe.pipe';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  events: IEvent[];
  amount;
  today;
  // day;
  month;
  // year;
  startOfTheYear;
  startOfTheMonth;
  daysForTheMonth = [];
  amountOfWeeks = [];
  //
  //
  currentDay;
  endOfTheMonth;

  constructor(private utilsService: UtilsService) {
    this.today = new Date();
    // this.day = new Date().getDate();
    this.month = new Date().getMonth();
    // this.year = new Date().getFullYear();
    this.startOfTheYear = new Date('2018-01-01');
    this.startOfTheMonth = new Date(this.today.getFullYear(), this.today.getMonth(), 1);
    this.endOfTheMonth = new Date(this.today.getFullYear(), this.today.getMonth() + 1, 1);
  }


  ngOnInit() {
    this.getEvents();
    this.daysForTheMonth = this.generateDayNumbers();
    this.amountOfWeeks = this.generateWeekLabels();
  }

  getEvents(): void {
    this.utilsService.getEvents()
      .subscribe(events => this.events = events);
  }

  generateDayNumbers() {
    const daysInTheMonth = this.getAmountOfDays(this.endOfTheMonth, this.startOfTheMonth, 86400000);
    const amountOfDays = [];
    for (let dayNumber = 1; dayNumber < daysInTheMonth + 1; dayNumber++) {
      amountOfDays.push(dayNumber);
    }
    return amountOfDays;
  }

  getAmountOfDays(biggerNumber, smallerNumber, divider) {
    this.amount = ((biggerNumber.getTime() - smallerNumber.getTime()) / divider).toFixed(0);
    return Number(this.amount);
  }

  getAmountOfWeeks(biggerNumber, smallerNumber, divider) {
    this.amount = ((biggerNumber.getTime() - smallerNumber.getTime()) / divider).toFixed(0);
    return Number(this.amount);
  }

  generateWeekLabels() {
    const weeksInTheMonth = this.getAmountOfWeeks(this.startOfTheMonth, this.startOfTheYear, 604800000);
    const amountOfWeeks = [];
    for (let weeks = weeksInTheMonth; weeks <  weeksInTheMonth + 5; weeks++) {
      amountOfWeeks.push(weeks);
    }
    return amountOfWeeks;
  }
}
