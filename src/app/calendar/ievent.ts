export interface IEvent {
  name: string;
  date: string;
  type: number;
  id: number;
}
