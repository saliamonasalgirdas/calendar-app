import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

import {IEvent} from './ievent';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  private eventUrl = 'http://localhost:3000/events';

  constructor(private http: HttpClient) {
  }

  getEvents(): Observable<IEvent[]> {
    return this.http.get<IEvent[]>(this.eventUrl);
  }
}
