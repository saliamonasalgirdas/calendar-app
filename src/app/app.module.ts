import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { CalendarComponent } from './calendar/calendar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DayComponent } from './calendar/day/day.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import {DayEventFilter} from './calendar/day/calendar-day-event/eventpipe.pipe';
import { ModalComponent } from './calendar/modal/modal.component';
import { CalendarDayEventComponent } from './calendar/day/calendar-day-event/calendar-day-event.component';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    NavbarComponent,
    DayComponent,
    SidebarComponent,
    DayEventFilter,
    ModalComponent,
    CalendarDayEventComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
